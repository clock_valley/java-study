class ForEach{
    public static void main(String[] args){
        int [] nums = {1, 3, 2, 5, 8};
        int sum = 0;
        //use for-each style
        for(int x: nums){
            System.out.println("Value is: " + x);
            sum += x;
        }
        System.out.println("Summation: " + sum);
    }
}