:PROPERTIES:
:ID:       f0e91359-57b5-4878-907e-f113f76f453f
:END:
#+title: Chapter 5 Closer Look at methods and classes

* Over loading methods
In Java, it is possible to define two or more methods within the same class that share the same name, as long as their parameter declartions are different. When this is the case, the methods are said to be *overloaded*, and the process is referred to as *method overloading*
- Method overloading supports polymorphism
#+begin_src java
class Overload{
    public static void main(String[] args){
        OverloadDemo ob = new OverloadDemo();
        double result;

        ob.test();
        ob.test(10);
        ob.test(10, 20);
        result = ob.test(123.25);
        System.out.println("Result of ob.test(123.25): " + result);
    }
}

class OverloadDemo{
    void test(){
        System.out.println("No parameter");
    }
    void test(int a){
        System.out.println("a: " + a);
    }
    void test(int a, int b){
        System.out.println("a and b: " + a + " " + b);
    }
    double test(double a){
        System.out.println("double a: " + a);
        return a*a;
    }
}
#+end_src

#+RESULTS:
: No parameter
: a: 10
: a and b: 10 20
: double a: 123.25
: Result of ob.test(123.25): 15190.5625

- Java implicitly type casts the overload, if same type not found. Overloaded methods only distinguise by number of arguments

* Overloaded constructors
#+begin_src java
class OverloadCons{
    public static void main(String[] args){
        BoxOverConst mybox1 = new BoxOverConst(10, 20, 15);
        BoxOverConst mybox2 = new BoxOverConst();
        BoxOverConst mycube = new BoxOverConst(7);

        double vol;

        vol = mybox1.volume();
        System.out.println("Volume of mybox1 is " + vol);

        vol = mybox2.volume();
        System.out.println("Volume of mybox2 is " + vol);

        vol = mycube.volume();
        System.out.println("Volume of mycube is " + vol);
    }
}

class BoxOverConst{
    double width;
    double height;
    double depth;

    BoxOverConst(){
        width = -1;
        height = -1;
        depth = -1;
    }
    BoxOverConst(double len){
        width = height = depth = len;
    }
    BoxOverConst(double w, double h, double d){
        width = w;
        height = h;
        depth = d;
    }
    double volume(){
        return width * height * depth;
    }
}
#+end_src

#+RESULTS:
: Volume of mybox1 is 3000.0
: Volume of mybox2 is -1.0
: Volume of mycube is 343.0

* Using object as parameter
#+begin_src java
class PassOb{
    public static void main(String[] args){
        Test ob1 = new Test(100, 22);
        Test ob2 = new Test(100, 22);
        Test ob3 = new Test(-1, -1);

        System.out.println("ob1 == ob2: " + ob1.equalTo(ob2));
        System.out.println("ob1 == ob3: " + ob1.equalTo(ob3));
    }
}

class Test{
    int a, b;

    Test(int i, int j){
        a = i;
        b = j;
    }
    boolean equalTo(Test o){
        if(o.a == a && o.b == b) return true;
        else return false;
    }
}
#+end_src

#+RESULTS:
: ob1 == ob2: true
: ob1 == ob3: false

* Arguments passing
- Passing primitive type is *passed by value*
  #+begin_src java
// Primitive types are passed by value
class CallByValue{
    public static void main(String[] args){
        Test ob = new Test();

        int a = 15, b = 20;

        System.out.println("a and b before call: " + a + " " + b);

        ob.meth(a, b);

        System.out.println("a and b after call: " + a + " " + b);
    }
}

class Test{
    void meth(int i, int j){
        i *= 2;
        j /= 2;
    }
}
#+end_src

#+RESULTS:
: a and b before call: 15 20
: a and b after call: 15 20

- Passing object as a method is *pass by reference*
#+begin_src java
// Objects are passed through their refereces.
class PassObjRef{
    public static void main(String[] args){
        Test ob = new Test(15, 20);

        System.out.println("ob.a and ob.b before call: " + ob.a + " " + ob.b);
        ob.meth(ob);

        System.out.println("ob.a and ob.b after call: " + ob.a + " " + ob.b);
    }
}
class Test{
    int a, b;

    Test(int i, int j){
        a = i;
        b = j;
    }

    void meth(Test o){
        o.a *= 2;
        o.b /= 2;
    }
}
#+end_src

#+RESULTS:
: ob.a and ob.b before call: 15 20
: ob.a and ob.b after call: 30 10

* Returning Objects

#+begin_src java
// Returning a object
class RetOb{
    public static void main(String[] args){
        Test1 ob1 = new Test1(2);
        Test1 ob2;

        ob2 = ob1.incrByTen();
        System.out.println("ob1.a: " + ob1.a);
        System.out.println("ob2.a: " + ob2.a);
    }
}

class Test1{
    int a;

    Test1(int i){
        a = i;
    }

    Test1 incrByTen(){
        Test1 temp = new Test1(a + 10);
        return temp;
    }
}
#+end_src

#+RESULTS:
: ob1.a: 2
: ob2.a: 12

* Recursion
Recursion is the process of defining something in terms of itself.
#+begin_src java
// A simple example of recusrion(Factorial)
class Recursion{
    public static void main(String[] args){
        Factorial f = new Factorial();

        System.out.println("Factorial of 8 is: " + f.fact(8));
    }
}

class Factorial{
    int fact(int n){

        if(n == 1) return 1;
        return n * fact(n - 1);
    }
}
#+end_src

#+RESULTS:
: Factorial of 8 is: 40320

#+begin_src java
// Another example that uses recusrion
class Recusrion2{
    public static void main(String[] args){
        RecTest ob = new RecTest(10);

        for(int i = 0; i < 10; i++) ob.values[i] = i;
        ob.printArray(10);
    }
}

class RecTest{
    int[] values;

    RecTest(int i){
        values = new int[i];
    }

    // display array -- recursively
    void printArray(int i){
        if(i == 0) return;
        else printArray(i - 1);
        System.out.println("[" + (i - 1) + "] " + values[i - 1]);
    }
}
#+end_src

#+RESULTS:
#+begin_example
[0] 0
[1] 1
[2] 2
[3] 3
[4] 4
[5] 5
[6] 6
[7] 7
[8] 8
[9] 9
#+end_example
[[id:ef683fd3-1c35-4639-8786-7e30fc6c3efd][Chapter 5.1 Closer look at methods and classes]]
