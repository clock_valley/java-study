// A simple example of recusrion(Factorial)
class Recursion{
    public static void main(String[] args){
        Factorial f = new Factorial();

        System.out.println("Factorial of 8 is: " + f.fact(8));
    }
}

class Factorial{
    int fact(int n){

        if(n == 1) return 1;
        return n * fact(n - 1);
    }
}