// Demonstrates public and private access modifier
class AccessTest{
    public static void main(String[] args){
        Test2 ob = new Test2();

        // These are ok
        ob.a = 10;
        ob.b = 20;

       //ob.c = 100; // Error!

        ob.setc(100);
        System.out.println("a, b and c: " + ob.a + " " + ob.b + " " + ob.getc());
    }
}
class Test2{
    int a;
    public int b;
    private int c;

    void setc(int i){
        c = i;
    }
    int getc(){
        return c;
    }
}