:PROPERTIES:
:ID:       307a73b5-d08c-4355-aff9-49bd658f32c7
:END:
#+title: Chapter 4 Classes

* Introduction
The class is the logical construct upon which entire Java language is built because it defines the shape and nature of an object.
- *Class*: A class is a template for an object
- *Object*: An object is an /instance/ of a class
The data, or variables, defined within a class are called /instance variables/.
#+begin_src java
// A program to demonstrate class
class BoxDemo{
    public static void main(String[] args){
        Box mybox = new Box();
        double vol;

        mybox.width = 10;
        mybox.height = 20;
        mybox.depth = 15;

        vol = mybox.width * mybox.height * mybox.depth;

        System.out.println("Volume is " + vol);
    }
}
class Box{
    double width;
    double height;
    double depth;
}
#+end_src


#+RESULTS:
: Volume is 3000.0

* Declaring Objects
When class is created, a new data type is created. Obtaining objects of a class is a two-step process.
- First, you must declare a variable that can refer to an object. This vairable doesn't define an object. Instead, it is simpy a variable that can refer to an object.
- Second, you must acquire an actual, physical copy of the object and assign it to that variable. You can do this using the *new* operator. The new operator dynamically allocates(that is, allocates at run time) memory for an object and returns a reference to it. This reference is, essentially, the address in memory of the object allocated by *new*. This reference is then stored int the variable. Thus, in Java, all class objects must be dynamically allocated
  #+begin_src java
    Box mybox; // declare reference to object
    mybox = new Box(); // allocate a Box object
  #+end_src
* Assigning Object Reference Variables
#+begin_src java
Box b1 = new Box();
Box b2 = b1;
#+end_src
Here b1 and b2 variable refer to the /same/ object. The assignment of b1 to b2 didn't allocate any memory or copy any part of the original object. It simpy makes *b2* refer to the same object as does *b1*. Thus any changes made to the object through *b2* will affect the object to whict *b1* is referring, since they are the same object.

Although b1 and b2 both refer to same object, they are not linked in any other way. For example, a subsequent assignment to b1 simply unhook b1 from the original object without affecting the object or affect b2.
#+begin_src java
Box b1 = new Box();
Box b2 = b1;
// ...
b1 = null;
// Here b1 has been set to null, but b2 still points to original object
#+end_src
* Methods
** Adding method to class
#+begin_src java
class BoxDemo{
    public static void main(String[] args){
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        mybox1.volume();

        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        mybox2.volume();
    }
}
class Box{
    double width;
    double height;
    double depth;

    void volume(){
        System.out.print("Volume is: ");
        System.out.println(width * height * depth);
    }
}
#+end_src

#+RESULTS:
: Volume is: 3000.0
: Volume is: 162.0

** Return a value
#+begin_src java
class BoxDemo{
    public static void main(String[] args){
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        vol = mybox1.volume();
        System.out.println("Volume is: " + vol);

        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        vol = mybox2.volume();
        System.out.println("Volume is: " + vol);
    }
}
class Box{
    double width;
    double height;
    double depth;

    double volume(){
        return width * height * depth;
    }
}

#+end_src

#+RESULTS:
: Volume is: 3000.0
: Volume is: 162.0

** Adding a method that takes parameters
#+begin_src java
class BoxDemo{
    public static void main(String[] args){
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        mybox1.setDim(10, 20, 15);
        vol = mybox1.volume();
        System.out.println("Volume is: " + vol);

        mybox2.setDim(3, 6, 9);
        vol = mybox2.volume();
        System.out.println("Volume is: " + vol);
    }
}

class Box{
    double width;
    double height;
    double depth;

    double volume(){
        return width * height * depth;
    }

    void setDim(double w, double h, double d){
        width = w;
        height = h;
        depth = d;
    }
}

#+end_src

#+RESULTS:
: Volume is: 3000.0
: Volume is: 162.0
* Constructors
A constructor is a special method which associated with the respective object. A constructor initialize an object immediately upon creation.
Once defined, the construct automatically called when the object is created,
before *new* operator completes.
Constructors don't have return type. This is because the implicit return type of a class constructor is the class type itself
Constructor can be parameterized.
#+begin_src java
class BoxDemo6{
    public static void main(String[] args){
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box(3, 6, 9);

        double vol;

        vol = mybox1.volume();
        System.out.println("Volume is " + vol);

        vol = mybox2.volume();
        System.out.println("Volume is " + vol);
    }
}

class Box{
    double width;
    double height;
    double depth;

    Box(double w, double h, double d){
        width = w;
        height = h;
        depth = d;
    }
    double volume(){
        return width * height * depth;
    }
}
#+end_src

#+RESULTS:
: Volume is 3000.0
: Volume is 162.0

** Default constructor
Java will implicitly add a constructor if no user defined constructor is defined.
** The /this/ keyword
The /this/ operator is used by method to refer the object that invoked it.
#+begin_src java
// A redundant use of this.
Box(double w, double h, double d){
    this.width = w;
    this.height = h;
    this.depth = d;
}
#+end_src

* Stack example
#+begin_src java
// Demonstration of stack

class TestStack{
    public static void main(String [] args){
        Stack mystack1 = new Stack();
        Stack mystack2 = new Stack();

        for(int i = 0; i < 10; i++) mystack1.push(i);
        for(int i = 10; i < 20; i++) mystack2.push(i);

        System.out.println("Stack in mystack1:");
        for(int i = 0; i < 12; i++)
            System.out.println(mystack1.pop());

        System.out.println("Stack in mystack1:");
        for(int i = 0; i < 10; i++)
            System.out.println(mystack2.pop());
    }
}
// This class defines an integer stack that can hold 10 values
class Stack{
    int [] stck = new int[10];
    int tos;

    Stack(){
        tos = -1;
    }

    void push(int item){
        if(tos == 9){
            System.out.println("Stack is full");
        }
        else
            stck[++tos] = item;
    }
    int pop(){
        if(tos < 0){
            System.out.println("Stack underflow");
            return 0;
        }
        else
            return stck[tos--];
    }
}
#+end_src

#+RESULTS:
#+begin_example
Stack in mystack1:
9
8
7
6
5
4
3
2
1
0
Stack underflow
0
Stack underflow
0
Stack in mystack1:
19
18
17
16
15
14
13
12
11
10
#+end_example
[[id:f0e91359-57b5-4878-907e-f113f76f453f][Chapter 5 Closer Look at methods and classes]]
