// this is comment
/* this is also comment */
/** this a multiline comment 
 * can have number of lines
*/
class HelloWorld{
    public static void main(String args []){
        String greeting = "Hello World";
        System.out.println(greeting);
        for(int i = 0; i < greeting.length(); i++){
            System.out.print("=");
        }
        System.out.println();
    }
}