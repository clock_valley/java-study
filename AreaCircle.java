public class AreaCircle {
    public static void main(String[] args) {
        double pi, r, a;
        r = 10.8;
        pi = 3.1416;
        a = pi * r * r;
        System.out.println("Area of circle is " + a);
    
        char ch1, ch2;
        ch1 = 88;
        ch2 = 'Y';
        System.out.println("ch1 contains " + ch1);
        System.out.println("ch2 contains " + ch2);
        
        // on char, arithmetic operations can be done
        char ch3;
        ch3 = ch2;
        ch3++;
        System.out.println("ch3 contains " + ch3);
    }
}
