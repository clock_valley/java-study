class OverloadCons{
    public static void main(String[] args){
        BoxOverConst mybox1 = new BoxOverConst(10, 20, 15);
        BoxOverConst mybox2 = new BoxOverConst();
        BoxOverConst mycube = new BoxOverConst(7);

        double vol;

        vol = mybox1.volume();
        System.out.println("Volume of mybox1 is " + vol);

        vol = mybox2.volume();
        System.out.println("Volume of mybox2 is " + vol);

        vol = mycube.volume();
        System.out.println("Volume of mycube is " + vol);
    }
}

class BoxOverConst{
    double width;
    double height;
    double depth;

    BoxOverConst(){
        width = -1;
        height = -1;
        depth = -1;
    }
    BoxOverConst(double len){
        width = height = depth = len;
    }
    BoxOverConst(double w, double h, double d){
        width = w;
        height = h;
        depth = d;
    }
    double volume(){
        return width * height * depth;
    }
}