// Demonstrates stack with access modifiers
class TestStackv1{
    public static void main(String[] args){
        Stackv1 mystack1 = new Stackv1();
        Stackv1 mystack2 = new Stackv1();

        for(int i = 0; i < 10; i++) mystack1.push(i);
        for(int i = 0; i < 20; i++) mystack2.push(i);

        System.out.println("Stack in mystack1:");
        for(int i = 0; i < 10; i++)
            System.out.println(mystack1.pop());

        System.out.println("Stack in mystack2:");
        for(int i = 0; i < 10; i++)
            System.out.println(mystack2.pop());
    }
}
class Stackv1{
 // This class defines an integer stack that can hold 10 values
    private int[] stck = new int[10];
    private int tos;

    Stackv1(){
        tos = -1;
    }
    void push(int item){
        if(tos == 9)
            System.out.println("Stack is full");
        else
            stck[++tos] = item;
    }
    int pop(){
        if(tos < 0){
            System.out.println("Stack underflow");
            return 0;
        }
        else
            return stck[tos--];
    }
}